import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

# Set matplotlib defaults

mpl.rcParams['font.sans-serif'] = 'Arial'
mpl.rcParams['legend.fontsize'] = 15
mpl.rcParams['xtick.labelsize'] = 15
mpl.rcParams['ytick.labelsize'] = 15
mpl.rcParams['axes.labelsize'] = 20

# Create the figure and axis
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal')

# Plot the centre line
ax.plot([0, 0], [-0.5, 3.5], 'k-.', linewidth=1.5)

# Plot the walls
ax.plot([-1, -1], [0, 3], 'k-', linewidth=1.5)
ax.plot([1, 1], [0, 3], 'k-', linewidth=1.5)

# Plot the top and bottom of the wall
x = np.linspace(-1,1)
yl = 0.07*(np.sin(3*x*np.pi) + np.sin(5*x*np.pi))
yu = yl + 3
ax.plot(x, yl, 'k-', linewidth=1.0)
ax.plot(x, yu, 'k-', linewidth=1.0)

# Annotate the wall locations
ax.plot([-1, -1], [3.05, 3.25], 'k-', linewidth=1.0)
ax.text(-1, 3.5, r'$x=-L$', fontsize=15, horizontalalignment='center', verticalalignment='top')
ax.plot([1, 1], [3.05, 3.25], 'k-', linewidth=1.0)
ax.text(1, 3.5, r'$x=L$', fontsize=15, horizontalalignment='center', verticalalignment='top')

# Annotate the axes
ax.arrow(0.0, -1.25, 0.5, 0.0, head_width=0.1, head_length=0.1, fc='k', ec='k')
ax.text(0.75, -1.0, r'$x$', fontsize=15, horizontalalignment='center', verticalalignment='top')
ax.arrow(0.0, -1.25, 0.0, 0.5, head_width=0.1, head_length=0.1, fc='k', ec='k')
ax.text(-0.25, -0.5, r'$y$', fontsize=15, horizontalalignment='center', verticalalignment='top')

# Annotate ambient conditions
ax.text(-1.7, 2, r'$T_\infty, h$', fontsize=15, horizontalalignment='center', verticalalignment='top')
ax.text(1.7, 2, r'$T_\infty, h$', fontsize=15, horizontalalignment='center', verticalalignment='top')

# Set the axis limits
ax.set_xlim(-2.5, 2.5)
ax.set_ylim(-1.5, 4)

# Remove the axes
plt.axis('off')

# Use tight layout
fig.tight_layout()

# Save the vector image
plt.savefig('PlaneWallSchematic.pdf', bbox_inches='tight')

# Show the plot
plt.show()
