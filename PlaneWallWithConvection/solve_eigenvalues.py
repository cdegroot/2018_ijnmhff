import numpy as np
from scipy.optimize import fsolve

# Function to solve
f = lambda x : x*np.tan(x) - 1.0

# Solve for the roots (initial values are set iteratively to get all of the roots)
x = fsolve(f, [1, 3, 6, 9, 12, 15, 18, 22], xtol = 1e-12)

print("[{:.12f}, {:.12f}, {:.12f}, {:.12f}, {:.12f}, {:.12f}, {:.12f}, {:.12f}]".format(*x))
