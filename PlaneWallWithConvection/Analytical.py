import numpy as np

# Parameters for analytical solution
zeta = np.array([0.860333589019,
                 3.425618459482,
                 6.437298179172,
                 9.529334405362,
                 12.645287223857,
                 15.771284874816,
                 18.902409956860,
                 22.036496727939])
C = 4*np.sin(zeta)/(2*zeta + np.sin(2*zeta))

def getSolution(Fo, x):
    """Function to return the analytical solution for a given Fourier number,
       Fo, at spatial positions x
    """
    # Compute analytical solution for x values in computed solution
    A = C*np.exp(-np.square(zeta)*Fo)
    T = np.zeros(x.size)
    dT = np.zeros(x.size)
    for i in range(zeta.size):
        z = zeta[i]
        T = T + A[i]*np.cos(z*x)
        c = np.cos(z)
        s = np.sin(z)
        t = np.tan(z)
        dzeta = 1/(t + z/c/c)
        dC = (4*c/(2*z + np.sin(2*z)) - 4*s*(2*np.cos(2*z) + 2)/np.square(2*z + np.sin(2*z)))*dzeta
        dA = (dC - 2*Fo*C[i]*z*dzeta)*np.exp(-np.square(z)*Fo)
        dT = dT + dA*np.cos(z*x) - A[i]*np.sin(z*x)*dzeta*x

    # Return solution and derivative
    return (T, dT)
