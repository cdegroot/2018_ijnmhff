import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import os, sys, inspect

# Import module to compute analytical solution
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
dir = os.path.dirname(currentdir)
sys.path.insert(0, dir)
import Analytical

# Set the plot style
plt.style.use('seaborn-deep')

# Residual tolerances
resids = ["1e-00", "1e-01", "1e-02", "1e-03", "1e-04", "1e-05", "1e-06", "1e-07"]

# Times at which to compare errors
times = [0.1, 0.3, 0.6, 1.0]

# Create dictionary to store error data
error = dict()
for time in times:
    error[str(time)] = dict()
    for name in ["T_max", "dT_max", "T_avg", "dT_avg"]:
        error[str(time)][name] = list()

# Loop through grid numbers
for resid in resids:
    # Loop through time values
    for time in times:
        # Read the computed solution
        file = os.path.join(resid, str(time) + '.csv')
        data = pd.read_csv(file, header=None, names=["x", "T", "dT"])
        data["T"] /= 100
        data["dT"] /= 100
        data.sort_values(by="x")

        # Get analytical solution for x values in computed solution
        T, dT = Analytical.getSolution(time, np.array(data["x"]))

        # Compute error measures
        T_error = np.array(data["T"]) - T
        dT_error = np.array(data["dT"]) - dT
        T_error_max = np.linalg.norm(T_error, np.inf)
        dT_error_max = np.linalg.norm(dT_error, np.inf)
        T_error_avg = np.mean(np.absolute(T_error))
        dT_error_avg = np.mean(np.absolute(dT_error))

        # Store the errors
        error[str(time)]["T_max"].append(T_error_max)
        error[str(time)]["dT_max"].append(dT_error_max)
        error[str(time)]["T_avg"].append(T_error_avg)
        error[str(time)]["dT_avg"].append(dT_error_avg)

# Create residual list as floats
res = list()
for resid in resids:
    res.append(float(resid))

# Create the figure and axis to plot avg error of temperature
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

ax1.semilogx(res, error["0.1"]["T_avg"], 'ko-', label="Fo = 0.1")
ax1.semilogx(res, error["0.3"]["T_avg"], 'kx-', label="Fo = 0.3")
ax1.semilogx(res, error["0.6"]["T_avg"], 'ks-', label="Fo = 0.6")
ax1.semilogx(res, error["1.0"]["T_avg"], 'k*-', label="Fo = 1.0")

ax1.set_xlim(1e-7, 1e-0)
ax1.set_ylim(3e-5, 3e-4)
ax1.grid(which='both')
ax1.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
ax1.set_xlabel(r"$r_{tol}$", fontsize='x-large')
ax1.set_ylabel(r"$\overline{E}_{\theta}$", fontsize='x-large')
legend = ax1.legend(loc=1, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Create the figure and axis to plot avg error of temperature derivative
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

ax2.semilogx(res, error["0.1"]["dT_avg"], 'ko-', label="Fo = 0.1")
ax2.semilogx(res, error["0.3"]["dT_avg"], 'kx-', label="Fo = 0.3")
ax2.semilogx(res, error["0.6"]["dT_avg"], 'ks-', label="Fo = 0.6")
ax2.semilogx(res, error["1.0"]["dT_avg"], 'k*-', label="Fo = 1.0")

ax2.set_xlim(1e-7, 1e-0)
ax2.set_ylim(3e-5, 3e-4)
ax2.grid(which='both')
ax2.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
ax2.set_xlabel(r"$r_{tol}$", fontsize='x-large')
ax2.set_ylabel(r"$\overline{E}_{\theta^\prime}$", fontsize='x-large')
legend = ax2.legend(loc=1, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Save the figures
fig1.savefig("TetResidTemperatureError.pdf")
fig2.savefig("TetResidDerivativeError.pdf")

# Show the figures
plt.show()
