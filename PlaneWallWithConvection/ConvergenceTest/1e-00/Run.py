import os, sys, inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
casedir = os.path.dirname(os.path.dirname(currentdir))
sys.path.insert(0, casedir)

import Case

Case.runCase("../../Meshes/tet2.msh", timeStep=0.001, totalTime=1.0, residTol=1e-0, writeConvergence=True)
