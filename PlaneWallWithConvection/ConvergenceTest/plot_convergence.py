import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pandas as pd
import csv
import os

# Set the plot style
plt.style.use('seaborn-deep')

# Residual tolerances
resids = ["1e-00", "1e-01", "1e-02", "1e-03", "1e-04", "1e-05", "1e-06", "1e-07"]

# Create dictionary to store convergence histories
convergence = dict()

# Loop through residual tolerances and read data
for resid in resids:
    # Read the residual history
    file = os.path.join(resid, 'convergence.csv')
    data = pd.read_csv(file, header=None, names=["time", "nIter"])
    data["time"] /= 1000
    convergence[resid] = data

# Create the figure and axis to plot avg error of temperature
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

# Define the linestyles and colours
linestyles = ['-', '--', '-.', ':', '-', '--', '-.', ':']
colours = ['k', 'k', 'k', 'k', 'grey', 'grey', 'grey', 'grey']

# Plot the data
for resid, linestyle, colour in zip(resids, linestyles, colours):
    label = r"$r_{tol} = 10^{-" + resid[-1] + r"}$"
    ax1.plot(convergence[resid]["time"], convergence[resid]["nIter"],
        color=colour, linestyle=linestyle, label=label)

# Configure the plot
ax1.set_xlim(0, 1)
ax1.set_ylim(0, 20)
ax1.yaxis.set_major_locator(MultipleLocator(5))
ax1.yaxis.set_minor_locator(MultipleLocator(1))
#ax1.grid(which='both')
ax1.set_xlabel(r"$Fo$", fontsize='x-large')
ax1.set_ylabel(r"Iterations to Convergence", fontsize='x-large')
legend = ax1.legend(loc=1, ncol=2, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Save the figures
fig1.savefig("TetConvergence.pdf")

# Show the figures
plt.show()
