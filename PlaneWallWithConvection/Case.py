from wediff.double import FwdDiff
from cfdframework import Equation
from cfdframework.FieldVariables import FieldVariables
from cfdframework.TransportCoeffs import TransportCoeffs
import cfdframework.fwd_diff_double as cfd
from cfdframework.fwd_diff_double import Vector
import csv

def runCase(meshName,
            timeStep=0.001,
            totalTime=1.0,
            residTol=1e-6,
            transient="SecondOrderEuler",
            writeVtk=False,
            writeConvergence=False):
    """Function to run a case for transient conduction in a plane wall"""

    # Initialize the time object
    cfd.Time().reset()

    # Read the mesh
    mesh = cfd.FluentMeshReader(meshName).read()
    mesh.reorder()

    # Define transport coefficients
    transportCoeffs = TransportCoeffs(cfd,
    {
        "mesh" : mesh,
        "density" :
        {
            "type" : "Constant",
            "config" :
            {
                "value" : cfd.FwdDiff(1.0)
            }
        },
        "thermalConductivity" :
        {
            "type" : "Constant",
            "config" :
            {
                "value" : cfd.FwdDiff(1.0)
            }
        }
    })

    # Define field variables
    fieldVariables = FieldVariables(cfd,
    {
        "mesh" : mesh,
        "T" :
        {
            "boundaryConditions" :
            {
                "symmetry" :
                {
                    "type" : "ZeroGradient",
                    "regions" : ["LEFT", "SYMM"],
                    "config" : None
                },
                "convection" :
                {
                    "type" : "Convection",
                    "regions" : ["RIGHT"],
                    "config" :
                    {
                        "h" : cfd.FwdDiff(1.0, [1.0]),
                        "Tinf" : cfd.FwdDiff(0.0),
                        "coeff" : transportCoeffs.getCoeff("thermalConductivity")
                    }
                }
            },
            "gradient" :
            {
                "type" : "Gauss",
                "nCorrectors" : 1,
                "interpolators" :
                {
                    "default" :
                    {
                        "type" : "Linear",
                        "regions" : ["int_FLUID"]
                    }
                }
            },
            "secondPassGradient" :
            {
                "type" : "Gauss",
                "nCorrectors" : 4,
                "interpolators" :
                {
                    "default" :
                    {
                        "type" : "LinearCorrected",
                        "regions" : ["int_FLUID"]
                    }
                }
            }
        }
    })

    energyEquation = Equation(cfd,
    {
        "name" : "energyEquation",
        "mesh" : mesh,
        "fieldVariables" : fieldVariables,
        "transportCoeffs" : transportCoeffs,
        "faceFluxes" :
        {
            "diffusion" :
            {
                "interior" :
                {
                    "type" : "Interior",
                    "regions" : ["int_FLUID"],
                    "config" :
                    {
                        "variable" : fieldVariables.getVariable("T"),
                        "coeff" : transportCoeffs.getCoeff("thermalConductivity")
                    }
                },
                "symmetry" :
                {
                    "type" : "Exterior",
                    "regions" : ["LEFT", "SYMM"],
                    "config" :
                    {
                        "variable" : fieldVariables.getVariable("T"),
                        "coeff" : transportCoeffs.getCoeff("thermalConductivity"),
                        "faceInterpolator" : fieldVariables.getBoundaryValueCalculator("symmetry")
                    }
                },
                "convection" :
                {
                    "type" : "Exterior",
                    "regions" : ["RIGHT"],
                    "config" :
                    {
                        "variable" : fieldVariables.getVariable("T"),
                        "coeff" : transportCoeffs.getCoeff("thermalConductivity"),
                        "faceInterpolator" : fieldVariables.getBoundaryValueCalculator("convection")
                    }
                }
            }
        },
        "transient" :
        {
            "type" : transient,
            "config" :
            {
                "variable" : "T",
                "coeff" : "density"
            }
        },
        "linearSolver" :
        {
            "variable" : "T",
            "relaxation" : 1.0
        }
    })

    # Initialize temperature field
    T0 = cfd.FwdDiff(100.0)
    T = energyEquation.getVariable("T")
    cellGroups = mesh.getCellGroups()
    for grp in cellGroups:
        num = mesh.getNumCellsInGroup(grp)
        for i in range(num):
            T.setAtCell(grp, i, T0)

    # Set up VTK file writer and write initial condition
    if writeVtk:
        vtkWriter = cfd.VtkWriter(mesh)
        vtkWriter.addVariable("T", T)
        vtkWriter.write("T_{:03}".format(0))

    # Open csv file for convergence history
    if writeConvergence:
        convergenceFile = open("convergence.csv", 'w')
        convergenceWriter = csv.writer(convergenceFile, delimiter=',')

    # Set up the timesteps
    cfd.Time().setNextTimestep(cfd.FwdDiff(timeStep))
    nTimeSteps = int(round(totalTime/timeStep))

    # Define output frequency
    outputFrequency = nTimeSteps//10

    # Initialize boundary values and gradient field
    energyEquation.initialize()

    # Loop through time
    for t in range(nTimeSteps):
        # Advance the timestep
        cfd.Time().advanceTime()

        # Report timestep information
        print(" Timestep = {}; Time = {} ".format(t, cfd.Time().getCurrentTime()))

        # Iterate the non-linear solution
        for i in range(50):
            # Solve the equation
            energyEquation.solve()

            # Report the residuals
            print("  Iteration = {}; Max. Resid = {}; Avg. Resid = {}".format(i,
                energyEquation.getMaxResidual(),
                energyEquation.getAvgResidual()))

            # Exit non-linear loop if converged
            if energyEquation.getMaxResidual().val() < residTol and energyEquation.getMaxResidual().dx(0) < residTol:
                if writeConvergence:
                    convergenceWriter.writerow([t+1,i+1])
                break

        # Write CSV file
        if (t + 1) % outputFrequency == 0:
            with (open("{:.1f}.csv".format(cfd.Time().getCurrentTime().val()), 'w')) as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                num = mesh.getNumCellsInGroup("FLUID")
                for j in range(num):
                    xc = mesh.getCell("FLUID", j).centroid()[0].val()
                    Tc = T.getAtCell("FLUID", j).val()
                    dTc = T.getAtCell("FLUID", j).dx(0)
                    writer.writerow([xc, Tc, dTc])
            if writeVtk:
                vtkWriter.write("T_{:03}".format(t+1))

    if writeConvergence:
        convergenceFile.close()
