import matplotlib as mpl
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import os, sys, inspect

# Import module to compute analytical solution
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
dir = os.path.dirname(currentdir)
sys.path.insert(0, dir)
import Analytical

# Set the plot style
plt.style.use('seaborn-deep')

# Grid numbers
grids = [0, 1, 2, 3]

# Numbers of control volumes
ncvs = [149, 397, 1282, 5813]

# Characteristic grid spacing
volume = 0.1*0.1
dx = np.power(volume/np.array(ncvs), 1.0/3.0)

# Times at which to compare errors
times = [0.1, 0.3, 0.6, 1.0]

# Create dictionary to store error data
error = dict()
for time in times:
    error[str(time)] = dict()
    for name in ["T_max", "dT_max", "T_avg", "dT_avg"]:
        error[str(time)][name] = list()

# Loop through grid numbers
for grid in grids:
    # Loop through time values
    for time in times:
        # Read the computed solution
        file = os.path.join(str(grid), str(time) + '.csv')
        data = pd.read_csv(file, header=None, names=["x", "T", "dT"])
        data["T"] /= 100
        data["dT"] /= 100
        data.sort_values(by="x")

        # Get analytical solution for x values in computed solution
        T, dT = Analytical.getSolution(time, np.array(data["x"]))

        # Compute error measures
        T_error = np.array(data["T"]) - T
        dT_error = np.array(data["dT"]) - dT
        T_error_max = np.linalg.norm(T_error, np.inf)
        dT_error_max = np.linalg.norm(dT_error, np.inf)
        T_error_avg = np.mean(np.absolute(T_error))
        dT_error_avg = np.mean(np.absolute(dT_error))

        # Store the errors
        error[str(time)]["T_max"].append(T_error_max)
        error[str(time)]["dT_max"].append(dT_error_max)
        error[str(time)]["T_avg"].append(T_error_avg)
        error[str(time)]["dT_avg"].append(dT_error_avg)

# Create the figure and axis to plot avg error of temperature
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)

ax1.loglog(dx, error["0.1"]["T_avg"], 'ko-', label="Fo = 0.1")
ax1.loglog(dx, error["0.3"]["T_avg"], 'kx-', label="Fo = 0.3")
ax1.loglog(dx, error["0.6"]["T_avg"], 'ks-', label="Fo = 0.6")
ax1.loglog(dx, error["1.0"]["T_avg"], 'k*-', label="Fo = 1.0")
ax1.loglog([0.01, 0.1], [1e-4, 1e-2], 'k--')
ax1.text(0.02, 1e-3, r"$\mathcal{O}(\Delta x^2)$",
         horizontalalignment='center', verticalalignment='center',
         bbox=dict(facecolor='white', edgecolor='black'),
         fontsize='large'
        )

ax1.set_xlim(0.01, 5e-2)
ax1.set_ylim(1e-5, 1e-2)
ax1.grid(which='both')
ax1.set_xlabel(r"$\Delta x^*$", fontsize='x-large')
ax1.set_ylabel(r"$\overline{E}_{\theta}$", fontsize='x-large')
legend = ax1.legend(loc=4, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Create the figure and axis to plot avg error of temperature derivative
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

ax2.loglog(dx, error["0.1"]["dT_avg"], 'ko-', label="Fo = 0.1")
ax2.loglog(dx, error["0.3"]["dT_avg"], 'kx-', label="Fo = 0.3")
ax2.loglog(dx, error["0.6"]["dT_avg"], 'ks-', label="Fo = 0.6")
ax2.loglog(dx, error["1.0"]["dT_avg"], 'k*-', label="Fo = 1.0")
ax2.loglog([0.01, 0.1], [1e-4, 1e-2], 'k--')
ax2.text(0.02, 1e-3, r"$\mathcal{O}(\Delta x^2)$",
         horizontalalignment='center', verticalalignment='center',
         bbox=dict(facecolor='white', edgecolor='black'),
         fontsize='large'
        )

ax2.set_xlim(0.01, 5e-2)
ax2.set_ylim(1e-5, 1e-2)
ax2.grid(which='both')
ax2.set_xlabel(r"$\Delta x^*$", fontsize='x-large')
ax2.set_ylabel(r"$\overline{E}_{\theta^\prime}$", fontsize='x-large')
legend = ax2.legend(loc=4, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Create the figure and axis to plot ratio of errors
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)

ratio0 = np.array(error["0.1"]["dT_avg"])/np.array(error["0.1"]["T_avg"])
ratio1 = np.array(error["0.3"]["dT_avg"])/np.array(error["0.3"]["T_avg"])
ratio2 = np.array(error["0.6"]["dT_avg"])/np.array(error["0.6"]["T_avg"])
ratio3 = np.array(error["1.0"]["dT_avg"])/np.array(error["1.0"]["T_avg"])

ax3.semilogx(ncvs, ratio0, 'ko-', label="Fo = 0.1")
ax3.semilogx(ncvs, ratio1, 'kx-', label="Fo = 0.3")
ax3.semilogx(ncvs, ratio2, 'ks-', label="Fo = 0.6")
#ax3.semilogx(ncvs, ratio3, 'k*-', label="Fo = 1.0")

ax3.set_ylim(0, 2)
ax3.set_xlabel(r"$N_{CV}$", fontsize='x-large')
ax3.set_ylabel(r"$\overline{E}_{\theta^\prime}/\overline{E}_{\theta}$", fontsize='x-large')
legend = ax3.legend(loc=3, framealpha=1, fontsize='x-large')
legend.get_frame().set_linewidth(1)
legend.get_frame().set_edgecolor("k")

# Save the figures
fig1.savefig("TetDiscretizationTemperatureError.pdf")
fig2.savefig("TetDiscretizationDerivativeError.pdf")
fig3.savefig("TetDiscretizationErrorRatio.pdf")

# Show the figures
plt.show()
