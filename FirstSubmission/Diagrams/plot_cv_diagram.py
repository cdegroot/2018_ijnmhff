import math
import matplotlib as mpl
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
import numpy as np

# Set matplotlib defaults

mpl.rcParams['font.sans-serif'] = 'Arial'
mpl.rcParams['legend.fontsize'] = 15
mpl.rcParams['xtick.labelsize'] = 15
mpl.rcParams['ytick.labelsize'] = 15
mpl.rcParams['axes.labelsize'] = 20

# Create the figure and axis
fig = plt.figure()
ax = fig.add_subplot(111, aspect='equal')

# Define the points (trace bottom face, then top face, in counter-clockwise direction)
p0 = np.array([0,0])
p1 = np.array([1,-0.1])
p2 = np.array([1.7, 0])
p3 = np.array([0.5, 0.2])
p4 = np.array([0.1, 0.7])
p5 = np.array([1.1, 0.6])
p6 = np.array([1.6, 0.7])
p7 = np.array([0.5, 0.8])

# Draw the control volume edges
#ax.plot([0, 1, 1.1, 0.1, 0], [0, -0.1, 0.6, 0.7, 0], 'k-', linewidth=1.5)
ax.plot([p0[0], p1[0], p5[0], p4[0], p0[0]], [p0[1], p1[1], p5[1], p4[1], p0[1]], 'k-', linewidth=1.5)
ax.plot([1, 1.7, 1.6, 1.1], [-0.1, 0, 0.7, 0.6], 'k-', linewidth=1.5)
ax.plot([1.6, 0.5, 0.1], [0.7, 0.8, 0.7], 'k-', linewidth=1.5)
ax.plot([0.5, 0.5, 1.7], [0.8, 0.2, 0], 'k--', linewidth=1.5)
ax.plot([0.5, 0], [0.2, 0], 'k--', linewidth=1.5)

# Shade the faces
patches = []
#bottom = Polygon(np.array([p0, p1, p2, p3]))
#patches.append(bottom)
#back = Polygon(np.array([p3, p2, p6, p7]))
#patches.append(back)
#left = Polygon(np.array([p0, p3, p7, p4]))
#patches.append(left)
right = Polygon(np.array([p1, p2, p6, p5]))
patches.append(right)
top = Polygon(np.array([p4, p5, p6, p7]))
patches.append(top)
front = Polygon(np.array([p0, p1, p5, p4]))
patches.append(front)

p = PatchCollection(patches, color='grey', alpha=0.2)
ax.add_collection(p)

# Annotate the wall locations
ax.text(0.55, 0.3, r'$\times$', fontsize=20, horizontalalignment='center', verticalalignment='center')
ax.text(0.8, 0.7, r'$\times$', fontsize=20, horizontalalignment='center', verticalalignment='center')

# Annotate the axes
#ax.arrow(0.0, -1.25, 0.5, 0.0, head_width=0.1, head_length=0.1, fc='k', ec='k')
#ax.text(0.75, -1.0, r'$x$', fontsize=15, horizontalalignment='center', verticalalignment='top')
#ax.arrow(0.0, -1.25, 0.0, 0.5, head_width=0.1, head_length=0.1, fc='k', ec='k')
#ax.text(-0.25, -0.5, r'$y$', fontsize=15, horizontalalignment='center', verticalalignment='top')

# Annotate ambient conditions
#ax.text(-1.7, 2, r'$T_\infty, h$', fontsize=15, horizontalalignment='center', verticalalignment='top')
#ax.text(1.7, 2, r'$T_\infty, h$', fontsize=15, horizontalalignment='center', verticalalignment='top')

# Set the axis limits
#ax.set_xlim(-2.5, 2.5)
#ax.set_ylim(-1.5, 4)

# Remove the axes
plt.axis('off')

# Use tight layout
fig.tight_layout()

# Save the vector image
#plt.savefig('PlaneWallSchematic.pdf', bbox_inches='tight')

# Show the plot
plt.show()
