\documentclass[preprint,authoryear,11pt]{elsarticle}
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lineno}
\usepackage{amssymb}
\usepackage{array}

\usepackage{adjustbox}
\usepackage{natbib}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{setspace}
\usepackage[a4paper,total={6.5in, 9in}]{geometry}

\journal{International Journal of Numerical Methods for Heat \& Fluid Flow}

\begin{document}
\begin{frontmatter}
\title{Convergence and Error Analysis for an Automatically Differentiated Finite-Volume-Based Heat Conduction Code}
\author[1]{Christopher T. DeGroot\corref{cor1}}
 \cortext[cor1]{Corresponding author}

\address[1]{Department of Mechanical and Materials Engineering, University of Western Ontario, London, Ontario, Canada}

\begin{abstract}
\noindent\textbf{Purpose} To investigate the convergence and error properties of a finite volume-based heat conduction code that automatic differentiation to evaluate derivatives of all solutions outputs with respect to arbitrary solution input(s). A problem involving conduction in a plane wall with convection at its surfaces is used as a test problem, since it has an analytical solution, and the error can be evaluated directly.

\noindent\textbf{Design/methodology/approach} The finite volume-method is used to discretize the transient heat diffusion equation with constant thermophysical properties. The discretized problem is then linearized using Newton's method, which results in two linear systems; one for the primary solution field and one for the secondary field, representing the derivative of the primary field with respect to the selected input(s). Derivatives required in the formation of the secondary linear system are obtained by automatic differentiation using an operator overloading and templating approach.

\noindent\textbf{Findings} The temporal and spatial discretization error for the derivative solution follows the same order of accuracy as the primary solution. Second order accuracy of the spatial and temporal discretization schemes is confirmed for both primary and secondary problems for both orthogonal and non-orthogonal grids. In non-orthogonal cases, convergence is found to be controlled by the secondary problem, which is found to have a larger residual than the primary problem.

\noindent\textbf{Originality/value} The convergence and error properties of derivative solutions obtained by automatic differentiation of finite volume-based codes has not been previously investigated.

\noindent\textbf{Article classification} Research paper
\end{abstract}

\begin{keyword}
Automatic differentiation \sep Finite volume method \sep Error \sep Accuracy \sep Convergence
\end{keyword}

\end{frontmatter}

\linenumbers

\section*{Nomenclature}

\begin{itemize}
\item[] $A_n$, coefficient in series expansion
\item[] $Bi$, Biot number
\item[] $c_p$ specific heat capacity $J/kg\cdot K$
\item[] $C_n$, coefficient in series expansion
\item[] $Fo$, Fourier number
\item[] $h$, convection coefficient, $W/m^2K$
\item[] $k$, thermal conductivity, $W/mK$
\item[] $L$, length of domain, $m$
\item[] $t$, time, $s$
\item[] $T$, temperature, $K$
\item[] $T_i$, initial temperature, $K$
\item[] $x$, spatial coordinate, $m$
\item[] $y$, spatial coordinate, $m$
\item[] $z$, spatial coordinate, $m$
\end{itemize}

\subsection*{Greek Symbols}

\begin{itemize}
\item[] $\alpha$, thermal diffusivity, $m^2/s$
\item[] $\zeta_n$, eigenvalue
\item[] $\theta$, dimensionless temperature
\item[] $\theta^\prime$, derivative of $\theta$ with respect to $Bi$
\item[] $\rho$, density, $kg/m^3$
\end{itemize}

\subsection*{Subscripts and Superscripts}

\begin{itemize}
\item[] $*$, dimensionless quantity
\item[] $\infty$, quantity associated with ambient condition
\end{itemize}

\section{Introduction}

Numerical solution of partial differential equations using the finite volume method is now routine in a number of industries, given the availability of a wide range of commercial and open source codes (e.g. OpenFOAM, which is described by \citet{Weller1998} and \citet{Jasak2007}). Along with more widespread use of numerical methods to solve problems involving heat and fluid flow, an interest to explore how numerical solutions vary as a function of their input parameters has developed \citep{Jemcov2004,Jemcov2009,Colin2006,Duvigneau2006,Bischof2007,DeGroot2018}. The most basic approach for evaluating solution sensitivities is by conducting multiple simulations and evaluating derivatives by finite differences, however, truncation and cancellation errors can become problematic if differences are either too large or too small \citep{Bischof1996,Griewank2008}. Other methods that can be used to determine solution sensitivities include the response surface method \citep{Shirvan2016,Wu2017}, the continuous sensitivity equation method \citep{Colin2006,Duvigneau2006}, and automatic differentiation.

Automatic differentiation is a numerical technique that can be used to accurately and efficiently evaluate derivatives of any value that is computed by a numerical algorithm \citep{Bischof1996,Giering1998,Griewank2008,Naumann2015}. The principle underlying automatic differentiation is that any algorithm can be considered as a series of operations on data by differentiable functions. As a result, the chain rule can, in principle, be used to compute derivatives of any computed value with respect to any other value upon which it depends.  In general, there are two distinct variations of automatic differentiation, namely the forward (or tangent) mode and the reverse (adjoint) mode, which are distinguished by the direction in which the chain rule is used to propagate derivative information. The advantage of reverse mode is that the computational cost is independent of the number of input variables, whereas in forward mode the cost is proportional to the number of input variables \citep{Griewank2008}. On the other hand, forward mode provides derivatives of all output variables (i.e. the full solution field and all intermediate values) without additional computational cost, whereas to cost of reverse mode grows with the number of output variables with respect to which derivatives are evaluated. As a result, reverse mode is most commonly used for geometric shape optimization problems since the number of unknowns, i.e.\ the boundary locations, is large, while the number of outputs, i.e.\ a cost function, is small. Forward mode is more suitable for problems with small numbers of inputs, and can be very useful in assessing solution uncertainties.

In addition to the ability to be applied in either forward or reverse mode, there are multiple ways in which automatic differentiation can be implemented into a code. The classical method is by source code transformation, where a program parses the original source code to generate a secondary code to compute the derivatives \citep{Bischof1996,Giering1998,Bischof2007,Hascoet2013}. While source transformation methods can produce an efficient secondary code, they do not support all programming language features. In particular, there is incomplete support for features such as object orientation, inheritance, and templating \citep{Hogan2014}, which are all essential features for developing modern software. An alternative approach is based on the idea of operator overloading, where a new data type is defined, along with all of its operators, such that it can propagate derivatives by chain rule through the code \citep{Aubert2001,Jemcov2004,Jemcov2009,Hogan2014,DeGroot2018}. This method has been applied successfully for forward mode differentiation of finite volume-based codes \citep{Jemcov2004,Jemcov2009,DeGroot2018}.

While it has been shown that forward mode automatic differentiation can be used to obtain sensitivities of solution fields with respect to arbitrary input parameters \citep{Jemcov2004,Jemcov2009,DeGroot2018}, the convergence and discretization error properties are not well known. Therefore, it is the focus of this work to study the spatial and temporal discretization errors in an automatically differentiated code for transient heat conduction problems. Specifically, the relationship between error in the derivative fields to the primary solution field will be explored. The test case is for transient heat conduction in a plane wall with convection at the surface, since this problem has an analytical solution that can be used for comparison. While the governing partial differential equation for this problem is linear, non-linearities in the solution can be introduced when the problem is solved on non-orthogonal grids, which require gradient corrections that depend on the solution field. The problem is first solved on orthogonal grids to separate non-linear effects from the effects of spatial and temporal discretization error. Then, the same problem is solved on non-orthogonal grids to study the convergence properties of both the primary and sensitivity solution algorithms.

\section{Numerical Methods}

\subsection{Governing Equation and Discretization}

In this study, a finite-volume-based code is used to solve the transient heat diffusion equation, given as
%
\begin{equation}
	\frac{\partial T}{\partial t} = \alpha\nabla^2T
    \label{eq:energy}
\end{equation}
%
where $T$ is temperature, $t$ is time, and $\alpha=k/\rho c_p$ is the constant thermal diffusivity of the material.

Equation \ref{eq:energy} is discretized by integrating over an arbitrary discrete polyhedral control volume in space, and over a discrete time interval. Let the space occupied by the control volume be denoted $\Omega_P$, and let the volume contained within $\Omega_P$ be $V_P$. Let us assume that $\Omega_P$ is bounded by the control surface $\partial \Omega_P$ which is the union of the discrete control surfaces $\partial \Omega_{ip}$. Each control surface is assumed to have an area $A_{ip}$, where $ip\in\{1,2,...,N_{ip} \}$ and $N_{ip}$ is the number of discrete control surfaces. An arbitrary control volume showing the nomenclature described is given in Fig.\ XX. Time integration is based on the idea of an integrated space-time domain, as described by \citet{Zwart1998}. In the present discretization integration is taken over a timestep of size $\Delta t$ from an initial time of $t - \Delta t/2$ to a final time of $t + \Delta t/2$, where the time value that is stored in the algorithm is at time $t$, as shown in Fig.\ XX. Integrating over space and time results in

\begin{equation}
	\frac{T_{P}^{t+\Delta t/2} - T_{P}^{t-\Delta t/2}}{\Delta t} V_{P}
	= \sum_{ip=1}^{N_{ip}} \alpha \left. \nabla T \right|_{ip} \cdot \mathbf{n}_{ip} A_{ip}
	\label{eq:disc_energy_1}
\end{equation}
%
where divergence theorem has been used to convert the volume integral of the diffusion term into a surface integral. Note that the subscript $P$ refers to values evaluated at the control volume centroid, while the subscript $ip$ refers to values evaluated at the centroid of the control surfaces. Time integration is fully-implicit, so the right side of Eq.\ \ref{eq:disc_energy_1} is assumed to be evaluated at the most current timestep, corresponding to time $t$. On the left side of Eq.\ \ref{eq:disc_energy_1}, the values at the time `faces' $t + \Delta t/2$ and $t - \Delta t/2$ must be evaluated using an appropriate interpolation scheme.

A first-order time integration scheme is obtained by interpolating the time face values assuming a piecewise constant distribution over each timestep, which is given as
%
\begin{subequations}
  \begin{equation}
    T_P^{t - \Delta t/2} = T_P^{t - \Delta t}
    \label{eq:first_order_time_minus}
  \end{equation}
  \begin{equation}
    T_P^{t + \Delta t/2} = T_P^t
    \label{eq:first_order_time_plus}
  \end{equation}
\end{subequations}
%
This time interpolations scheme is analogous to an upwind scheme for spatial interpolations.

A second-order time integration scheme is obtained by interpolating the time face values by assuming a piecewise linear distribution between time values. This results in
%
\begin{subequations}
  \begin{equation}
    T_P^{t - \Delta t/2} = T_P^{t - \Delta t} + \frac{1}{2}\left(T_P^{t - \Delta t} - T_P^{t - 2\Delta t} \right)
    \label{eq:second_order_time_minus}
  \end{equation}
  \begin{equation}
    T_P^{t + \Delta t/2} = T_P^t + \frac{1}{2}\left(T_P^t - T_P^{t - \Delta t} \right)
    \label{eq:second_order_time_plus}
  \end{equation}
\end{subequations}

The diffusive flux is computed based on the method shown in \citet{DeGroot2018}, which, for the case of constant thermal diffusivity reduces to
%
\begin{multline}
	\left. \nabla T \right|_{ip,P} \cdot \mathbf{n}_{ip} =
	\frac{T_{nb} - T_P}{(\mathbf{D}_{P,ip}\cdot\mathbf{n}_{ip}) - (\mathbf{D}_{nb,ip}\cdot\mathbf{n}_{ip})}
	+ \frac{(\mathbf{D}_{nb,ip}-(\mathbf{D}_{nb,ip}\cdot\mathbf{n}_{ip})\mathbf{n}_{ip})}
	{(\mathbf{D}_{P,ip} \cdot \mathbf{n}_{ip}) - (\mathbf{D}_{nb,ip} \cdot \mathbf{n}_{ip})} \cdot\left.\nabla T \right|_{nb} \\
	- \frac{(\mathbf{D}_{P,ip}-(\mathbf{D}_{P,ip}\cdot\mathbf{n}_{ip})\mathbf{n}_{ip})}
	{(\mathbf{D}_{P,ip} \cdot \mathbf{n}_{ip}) - (\mathbf{D}_{nb,ip} \cdot \mathbf{n}_{ip})} \cdot\left.\nabla T \right|_P
	\label{eq:norm_der_ip_2}
\end{multline}
%
where the geometric parameters are shown in Fig.\ XXX. The two gradient terms in \ref{eq:norm_der_ip_2} account for non-orthogonality in the grid.

\subsection{Gradient Reconstruction}

Cell-centered gradients are reconstructed using Gauss' theorem, i.e.
%
\begin{equation}
    \left. \nabla\phi \right|_P = \frac{1}{V_P} \sum_{ip=1}^{N_{ip}} \phi_{ip} \mathbf{n}_{ip} A_{ip}
    \label{eq:GaussGradient}
\end{equation}
%
where $\phi$ is a generic scalar quantity. In order to fully define the gradient reconstruction method, the interpolation method for $\phi_{ip}$ must be specified. In this study, the gradients are computed in two phases. The first phase uses a simple inverse distance approximation to the point $f$, which lies on the vector connecting the cell centroids $P$ and $nb$. The point $f$ is defined such that the vector $\mathbf{D}_{f,ip}$ is orthogonal to the vector $\mathbf{D}_{P,nb}$, which minimizes the overall size of the gradient correction term and ensures that the correction term is zero on an orthogonal grid. The inverse distance interpolation used in the first gradient reconstruction phase is defined as
%
\begin{equation}
    \phi_{ip} \approx \left(1 - f\right) \phi_P + f \phi_{nb}
    \label{eq:InverseDistanceInterp}
\end{equation}

In this phase, the value is not corrected to the integration point $ip$ since the gradient values required to do so are not accurate. Further, it was reported by \citet{Betchen2009} that implementing gradient corrections within a simple iterative scheme takes an impractical number of iterations to converge. After the first phase, a low-order estimate of the gradients are obtained which are used to repeat the procedure with gradient corrections to the integration point. The corrected value at the integration point is \citep{Betchen2009}
%
\begin{equation}
    \phi_{ip} \approx \left(1 - f\right) \phi_P + f \phi_{nb}
    + \left[ \left(1 - f\right) \left.\nabla\phi\right|_P + f \left.\nabla\phi\right|_{nb} \right] \cdot \mathbf{D}_{f,ip}
    \label{eq:CorrectedInterp}
\end{equation}
%
The second gradient reconstruction phase is repeated for a fixed number of iterations to further improve the interpolations to the integration point using more accurate gradient values. In the present study a total of four iterations in the second gradient reconstruction phase was found to be sufficient. For a given iteration in the gradient reconstruction, the gradient values from the previous iteration were used for all interpolations, even if one of the neighbouring cell gradients had already been updated within the present iteration. This was done to improve convergence and stability of the gradient reconstruction. It should be noted that this is in contrast to the iterative gradient reconstruction method of \citet{Karimian2006}, reported on by \citet{Betchen2009}, which uses the most up-to-date gradient values at all times and was found to exhibit poor convergence and stability.

\subsection{Linearization and Primary Solution Method}

By taking all terms in the discretized equation onto one side of the equation, the residual for the cell $P$ can be expressed as
%
\begin{equation}
    r_P = \frac{T_{P}^{t+\Delta t/2} - T_{P}^{t-\Delta t/2}}{\Delta t} V_{P}
	- \sum_{ip=1}^{N_{ip}} \alpha \left. \nabla T \right|_{ip} \cdot \mathbf{n}_{ip} A_{ip}
\end{equation}
%
where each term is further specified by the temporal and spatial discretization schemes described previously. Taking the residual at each cell, the vector $\mathbf{r}\in\mathbb{R}^N$ is defined, where $N$ is the number of cells in the domain. Similarly, the vector of temperature values at each cell is defined as $\mathbf{T}\in\mathbb{R}^N$. Since the residual depends on the temperature field, the residual vector computed at a particular iteration $i$ using the temperature vector $\mathbf{T}_i$ is denoted $\mathbf{r}\left(\mathbf{T}_i\right)$. In a converged solution, $\mathbf{r}=0$, which is used as the basis of forming the Newton iteration to solve the linearized problem. The linear system to be solved is \citep{DeGroot2018}
%
\begin{equation}
    \mathbf{J}\left(\mathbf{T}_i\right)\Delta\mathbf{T}_i = - \mathbf{r}\left(\mathbf{T}_i\right)
    \label{eq:PrimaryLinearSystem}
\end{equation}
%
where $\mathbf{J}$ is the Jacobian matrix, which is defined as
\begin{equation}
    \mathbf{J}(\mathbf{T}) = \frac{\partial\mathbf{r}}{\partial\mathbf{T}}
    \label{eq:PrimaryJacobian}
\end{equation}
%
and the fixed point iteration that defines the solution advancement is given as
where $\mathbf{J}$ is the Jacobian matrix, which is defined as
%
\begin{equation}
    \mathbf{T} = \mathbf{T}_i + \Delta\mathbf{T}_i
    \label{eq:PrimaryFixedPoint}
\end{equation}
%
where $\mathbf{T}$ is the updated solution field, which becomes $\mathbf{T}_i$ in the next iteration. The linear system defined by Eq.\ \ref{eq:PrimaryLinearSystem} is solved using PETSc libraries \citep{petsc-efficient,petsc-user-ref,petsc-web-page}.

\subsection{Automatic Differentiation and Secondary Solution Method}

The secondary solution refers to the derivative(s) of the temperature field with respect to specified input parameter (or set of parameters). For simplicity, the case of a single input parameter and a single derivative field will be discussed, which is denoted by $\mathbf{T}^\prime$, where the prime symbol denotes differentiation with respect to the specified input parameter, $\psi$. The linear system to be solved for the sensitivities is defined by differentiating Eqs.\ \ref{eq:PrimaryLinearSystem} and \ref{eq:PrimaryFixedPoint} with respect to $\psi$, resulting in \citep{DeGroot2018}
%
\begin{equation}
    \mathbf{J}\left(\mathbf{T}_i\right)\Delta\mathbf{T}_i^\prime = - \mathbf{r}^\prime\left(\mathbf{T}_i\right)
    - \mathbf{J}^\prime\left(\mathbf{T}_i\right)\Delta\mathbf{T}_i
    \label{eq:SecondaryLinearSystem}
\end{equation}
%
and
%
\begin{equation}
    \mathbf{T}^\prime = \mathbf{T}_i^\prime + \Delta\mathbf{T}_i^\prime
    \label{eq:SecondaryFixedPoint}
\end{equation}
%
The derivative quantities in the secondary linear system, namely $\mathbf{r}^\prime\left(\mathbf{T}_i\right)$ and $\mathbf{J}^\prime\left(\mathbf{T}_i\right)$ are computed using automatic differentiation, implemented using operator overloading and templating of the code. The open source package \texttt{WEdiff} \citep{WEdiff} has been used for automatic differentiation, since it provides methods for both the C++ and Python programming languages, which is suitable for this work since it mixes both of these languages as well.

\section{Results and Discussion}

\subsection{Problem Definition and Analytical Solution}

The problem to be solved involves transient heat conduction in a symmetric domain of length $2L$, exposed to convective conditions at $x = \pm L$, as shown in Fig.\ \ref{fig:PlaneWallSchematic}. The wall is considered to be at an initially uniform temperature of $T = T_i$ when it is instantaneously exposed to an external fluid with an ambient temperature of $T_\infty$ and a convection coefficient of $h$. Due to the surface heat flux $q = h \left[T(L,t) - T_\infty \right]$, the temperature of the wall decreases in time. The dimensions of the wall in the $y$ and $z$ (into the page) directions are considered to be much larger than $L$, such that the problem becomes one-dimensional.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=250pt]{Figures/PlaneWallSchematic.pdf}
	\caption{Schematic diagram of a plane wall with convective boundary conditions at its outer surfaces.}
	\label{fig:PlaneWallSchematic}
\end{figure}

This problem can be solved analytically using a Fourier series \citep{Incropera2007}. Before presenting the analytical solution, various dimensionless parameters must be defined. The spatial coordinate $x$ is non-dimensionalized as
%
\begin{equation}
	x^* = \frac{x}{L}
    \label{eq:x_nondim}
\end{equation}
%
while time is represented non-dimensionally as the Fourier number, $Fo$, which is defined as
%
\begin{equation}
	Fo = \frac{\alpha t}{L^2}
    \label{eq:fourier}
\end{equation}
%
The dimensionless temperature is defined as
%
\begin{equation}
	\theta =
    \frac{T-T_\infty}{T_i-T_\infty}
    \label{eq:theta}
\end{equation}
%
The Biot number, $Bi$, which characterizes the ratio of conductive to convective resistances, is defined as
%
\begin{equation}
	Bi = \frac{hL}{k}
    \label{eq:biot}
\end{equation}

The analytical solution, given in terms of the dimensionless parameters above, is given as
%
\begin{equation}
	\theta =
    \sum_{n=1}^\infty
    C_n
    \exp\left(-\zeta_n^2 Fo\right)
    \cos\left(\zeta_n x^*\right)
    \label{eq:wall_analytical}
\end{equation}
%
where the values $\zeta_n$ are the eigenvalues of the problem, where are defined by the positive roots of
%
\begin{equation}
	\zeta_n \tan\zeta_n = Bi
	\label{eq:zeta_eqn}
\end{equation}
%
The coefficients $C_n$ are defined in terms of $\zeta_n$ as
%
\begin{equation}
	C_n = \frac{4\sin\zeta_n}{2\zeta_n + \sin\left( 2 \zeta_n \right)}
    \label{eq:cn_eqn}
\end{equation}

Since the aim of this work is to assess errors between numerical and analytical solutions, it is necessary to ensure that any errors in the analytical solution, due to truncation of the Fourier series are minimized. Therefore, the eigenvalues, $\zeta_n$, are determined precisely using the \texttt{fsolve} function within the \texttt{scipy.optimize} Python module with a tolerance (\texttt{xtol}) of $10^{-12}$. Additionally, eight eigenvalues are included in the series solution rather than the four that are recommended for $Fo < 0.2$ by \citet{Incropera2007}. The eigenvalues included in the analytical solution are summarized in Table \ref{tab:eigenvalues}. By examining the contribution of each Fourier mode, it can be verified that the last few modes contribute very little to the solution, confirming that no further accuracy would be attained by including more modes.

\begin{table}[htbp]
\centering
\caption{Summary of the eigenvalues determined numerically for the analytical solution of the plane wall conduction problem.}
\label{tab:eigenvalues}
\begin{tabular}{c c}
 \hline
 Eigenvalue & Numerical Value \\
 \hline
 $\zeta_1$ & 0.860333589019 \\
 $\zeta_2$ & 3.425618459482 \\
 $\zeta_3$ & 6.437298179172 \\
 $\zeta_4$ & 9.529334405362 \\
 $\zeta_5$ & 12.645287223857 \\
 $\zeta_6$ & 15.771284874816 \\
 $\zeta_7$ & 18.902409956860 \\
 $\zeta_8$ & 22.036496727939 \\
 \hline
\end{tabular}
\label{table:1}
\end{table}

An analytical solution for the derivative of the dimensionless temperature field, $\theta$, with respect to $Bi$, has been previously derived by \citet{DeGroot2018} and is summarized as
%
\begin{equation}
	\theta^\prime =
    \frac{\partial \theta}{\partial Bi} =
    \sum_{n=1}^\infty
    \left[
    \frac{\partial A_n}{\partial Bi}
    \cos\left(\zeta_n x^*\right)
    - A_n
    \sin\left(\zeta_n x^*\right)
    \frac{\partial \zeta_n}{\partial Bi}
    x^*
    \right]
    \label{eq:deriv_T}
\end{equation}
%
where
%
\begin{equation}
	A_n = C_n \exp\left(-\zeta_n^2 Fo\right)
\end{equation}
%
\begin{equation}
	\frac{\partial A_n}{\partial Bi} =
    \left[
    \frac{\partial C_n}{\partial Bi}
    - 2 Fo C_n \zeta_n \frac{\partial \zeta_n}{\partial Bi}
    \right]
    \exp\left(-\zeta_n^2 Fo\right)
    \label{eq:deriv_An}
\end{equation}
%
\begin{equation}
	\frac{\partial C_n}{\partial Bi} =
    \left[
    \frac{4\cos\zeta_n}{2\zeta_n + \sin(2\zeta_n)}
    - \frac{4\sin\zeta_n\left[2\cos(2\zeta_n)+2\right]}{\left[2\zeta_n + \sin(2\zeta_n)\right]^2}
    \right]
    \frac{\partial \zeta_n}{\partial Bi}
    \label{eq:deriv_Cn}
\end{equation}
%
\begin{equation}
	\frac{\partial \zeta_n}{\partial Bi} =
    \frac{1}{\tan\zeta_n + \zeta_n \sec^2\zeta_n}
    \label{eq:deriv_zetan}
\end{equation}

\subsection{One-Dimensional Numerical Results}

Numerical results are obtained using three-dimensional grids with a single control volume in each of the $y$ and $z$ directions. In the $x$ direction, the number of control volumes is varied to examine the spatial discretization error. Since the solution is symmetric about $x = 0$, the computational domain is taken to extend from $x = 0$ to $x = L$. The boundary condition at $x = 0$ enforces symmetry, while the boundary condition at $x = L$ implements the specified convective heat flux. On the $y$ and $z$ planes, symmetry boundary conditions are applied. The Biot number for all cases shown is $Bi = 1$ and solutions are computed until $Fo = 1$. These parameters are the same as those used in \citep{DeGroot2018}, which can be referred to for qualitative comparisons between the numerical and analytical solutions for $\theta$ and $\theta^\prime$. The focus of this work is to quantify the spatial and temporal discretization errors and to specifically determine how the error in sensitivity field(s) is related to the primary solution error.

The average solution error is examined by taking the mean magnitude of the difference between the analytical and numerical solutions at each control volume in the grid. For a numerical solution quantity $\phi$ with analytical solution $\hat{\phi}$, the mean error is defined as

\begin{equation}
	\overline{E}_\phi =
    \frac{1}{N_{CV}} \sum_{i=0}^{N_{CV}-1} \left| \hat{\phi}_i - \phi_i \right|
    \label{eq:average_error_theta}
\end{equation}
%
where $N_{CV}$ is the number of control volumes used to discretize the domain and $i$ refers to the index of the control volume. In this study, the computed errors are those for the dimensionless fields $\theta$ and $\theta^\prime$ with errors $\overline{E}_\theta$ and $\overline{E}_{\theta^\prime$}, respectively.

\subsubsection{Spatial Discretization Error}

To evaluate the relationship between spatial discretization error and grid resolution, a series of increasingly refined grids, with uniform grid spacing, are used to compute the $\theta$ and $\theta^\prime$ solutions. The number of control volumes in the $x$ direction, denoted $N_{CV}$, follows the pattern $N_{CV} = 2^n$, where $n \in \mathbb{Z} : n \in [1, 7]$ and $\mathbb{Z}$ is the set of integers. The number of control volumes in the $y$ and $z$ directions are each one.  To avoid confounding the effects of temporal discretization error, a small dimensionless timestep of $\Delta t^* = \alpha \Delta t / L^2 = 10^{-4}$ was used for all spatial discretization error test cases. The results of the spatial discretization error study are shown in Fig.\ \ref{fig:DiscretizationErrorResults}. For both the primary solution, $\theta$, and the derivative solution, $\theta^\prime$, it is observed that the mean error reduces in proportion with $N_{CV}^2$ (i.e.\ inversely proportional to $\Delta x^2$, where $\Delta x$ is the grid spacing). This shows that the theoretical second-order spatial accuracy of the numerical method is attained for both primary and secondary problems.

\begin{figure}[htbp]
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/DiscretizationTemperatureError.pdf}
    \caption{}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/DiscretizationDerivativeError.pdf}
    \caption{}
  \end{subfigure}
%
\caption{Plots of the average error between the numerical and analytical solutions at different Fourier numbers for (a) the dimensionless temperature field, $\theta$, and (b) the derivative of $\theta$ with respect to the Biot number, denoted $\theta^\prime$, as functions of the number of control volumes used to discretize the length of the domain.}
\label{fig:DiscretizationErrorResults}
\end{figure}

The error is shown on each plot for four different Fourier numbers, representing different instants in time. Overall, the errors are found to follow the same trend regardless of $Fo$, with the errors in the secondary problem being more closely aligned than those for the primary problem. It is noted that the largest error is observed for $Fo=0.1$, which could be due to the fact that the first timestep can only use a first-order time integration scheme due to a lack of data for earlier timesteps. As time advances to $Fo=0.3$, this start-up effect seems to be washed out as the error is minimized. As time goes on to $Fo=0.6$ and $Fo=1.0$, the error increases, likely due to an accumulation of temporal discretization errors.

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.5\linewidth]{Figures/DiscretizationErrorRatio.pdf}
\caption{Caption.}
\label{fig:DiscretizationErrorRatio}
\end{figure}

Figure \ref{fig:DiscretizationErrorRatio} shows the ratio of the mean error in the secondary problem to that for the primary problem, i.e.\ $\overline{E}_{\theta^\prime}/\overline{E}_{\theta}$. It is observed that the errors are of the same magnitude; in this case, the error in the secondary problem is always more than the primary problem, but is no more than twice as large.

\subsubsection{Temporal Discretization Error}

Timestep details.
Mesh used.

\begin{figure}[htbp]
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TemporalFirstOrderTemperatureError.pdf}
    \caption{}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TemporalFirstOrderDerivativeError.pdf}
    \caption{}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TemporalSecondOrderTemperatureError.pdf}
    \caption{}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TemporalSecondOrderDerivativeError.pdf}
    \caption{}
  \end{subfigure}
%
\caption{Plots of the average error between the numerical and analytical solutions at different Fourier numbers for (a,b) the dimensionless temperature field, $\theta$, and (b,d) the derivative of $\theta$ with respect to the Biot number, denoted $\theta^\prime$, as functions of the timestep used for temporal discretization using a (a,b) first order scheme and (c,d) a secon order scheme.}
\label{fig:TemporalErrorResults}
\end{figure}

\subsection{Effects of Grid Non-Orthogonality}

\begin{figure}[htbp]
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TetResidTemperatureError.pdf}
    \caption{}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.5\linewidth}
  	\centering
	\includegraphics[width=\linewidth]{Figures/TetResidDerivativeError.pdf}
    \caption{}
  \end{subfigure}
%
\caption{Plots of the average error between the numerical and analytical solutions at different Fourier numbers for (a) the dimensionless temperature field, $\theta$, and (b) the derivative of $\theta$ with respect to the Biot number, denoted $\theta^\prime$, as functions of the residual tolerance for the linearization iterations.}
\label{fig:DiscretizationErrorResults}
\end{figure}

\section{Summary}


\section*{Acknowledgements}

Funding: This work was supported by the Natural Science and Engineering Research Council (NSERC) [RGPIN-2017-04078].

\section{References}
\bibliographystyle{elsarticle-harv}
\bibliography{References}
\end{document}
